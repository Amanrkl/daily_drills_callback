
// NOTE: Do not change the name of this file

// NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

const fs = require('fs');
const path = require('path');

function readFile(fileName) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to read ${fileName}`);
        fs.readFile(filePath, "utf-8", (err, data) => {
            if (err) {
                console.error(`Error in reading ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in reading ${fileName}`);
                resolve(data)
            }
        })
    });
}

function createFile(fileName, dataToWrite) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to write in ${fileName}`);
        fs.writeFile(filePath, dataToWrite, (err) => {
            if (err) {
                // console.error(`Error in writing in ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in writing in ${fileName}`);
                resolve(fileName)
            }
        })
    });
}

function deleteFile(fileName) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to delete  ${fileName}`);
        fs.unlink(filePath, (err) => {
            if (err) {
                // console.error(`Error in deleting ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in deleting ${fileName}`);
                resolve(fileName)
            }
        })
    });
}

function appendFile(fileName, dataToAppend) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to append in ${fileName}`);
        fs.appendFile(filePath, dataToAppend + "\n", (err) => {
            if (err) {
                console.error(`Error in appending to ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in appending to ${fileName}`);
                resolve(fileName)
            }
        })
    });
}
// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

function problem1() {
    const file1 = createFile("sample1.txt", "Hello");
    const file2 = createFile("sample2.txt", "World");
    setTimeout(() => {
        [file1, file2].map(file => {
            file
                .then(fileName => deleteFile(fileName))
                .catch(err => console.log("Error found", err))
        })
    }, 2 * 1000)
}

// problem1()


// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining

function problem2(originalFile, duplicateFile) {
    readFile(originalFile)
        .then(data => createFile(duplicateFile, data))
        .then(_ => deleteFile(originalFile))
        .catch(err => console.log("Error Found", err))
}

// problem2("lipsum.txt", "copylipsum.txt")


// Q3.

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    const userdata = {
        "user": user,
        "activity": activity,
        "timestamp": Date.now()
    }
    appendFile("userlog.txt", JSON.stringify(userdata))
        .catch(err => console.log(err))
}

const user1 = {
    name: "Aman Singhal",
    age: 25,
    gender: "Male",
    job_profile: "SDE1"
}

const user2 = {
    name: "Shivam Sethi",
    age: 27,
    gender: "Male",
    job_profile: "APM"
}


// A. login with value 3 and call getData once login is successful
function problem3A(user, value) {
    login(user, value)
        .then(_ => getData())
        .catch(err => console.error(err))
}


// problem3A(user1, 3)

/*
B. Write logic to logData after each activity for each user. Following are the list of activities
"Login Success"
"Login Failure"
"GetData Success"
"GetData Failure"

Call log data function after each activity to log that activity in the file.
All logged activity must also include the timestamp for that activity.

You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
All calls must be chained unless mentioned otherwise.

*/
function problem3B(user, value) {
    login(user, value)
        .then(user => {
            logData(user, "Login Success")
            getData()
                .then(_ => logData(user, "GetData Success"))
                .catch(_ => logData(user, "GetData Failure"))
        })
        .catch(_ => logData(user, "Login Failure"))
}

// problem3B(user2, 3)
// problem3B(user1, 6)