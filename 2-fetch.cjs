/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
const fs = require("fs");
const path = require("path");

const userUrl = "https://jsonplaceholder.typicode.com/users";
const todosUrl = "https://jsonplaceholder.typicode.com/todos";

function loadData(url) {
    return fetch(url)
        .then((response) => {
            if (response.ok) {
                return response.json()
            } else {
                return new Error("Url not found")
            }
        })
}

function writeFile(fileName, data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(path.join(__dirname, fileName), data, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log(`Sucessful in writing in file ${fileName}`);
                resolve();
            }
        });
    });
}

//   1. Fetch all the users

loadData(userUrl)
    .then((usersdata) => writeFile("users.json", JSON.stringify(usersdata)))
    .catch((err) => console.error(err));

// 2. Fetch all the todos

loadData(todosUrl)
    .then((data) => writeFile("todos.json", JSON.stringify(data)))
    .catch((err) => console.error(err));

// 3. Use the promise chain and fetch the users first and then the todos.

loadData(userUrl)
    .then((_) => loadData(todosUrl))
    .then((_) => console.log("Task Successful"))
    .catch((err) => console.error(err));

// 4. Use the promise chain and fetch the users first and then all the details for each user.

loadData(userUrl)
    .then(users => {
        return Promise.all(users.reduce((detailsOfEachUser, user) => {
            detailsOfEachUser.push(loadData(path.join(userUrl, `?id=${user.id}`)))
            return detailsOfEachUser
        }, []))
    })
    .then((data) => writeFile("usersdetails.json", JSON.stringify(data)))
    .catch((err) => console.error(err));


// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

loadData(todosUrl)
    .then(tasks => {
        let firsttasks = tasks[0];
        return loadData(path.join(userUrl, `?id=${firsttasks.userId}`))
    })
    .then((data) => writeFile("userdetailsOfFirstTodos.json", JSON.stringify(data)))
    .catch((err) => console.error(err));