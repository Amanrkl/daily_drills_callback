/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    

    NOTE: Do not change the name of this file

*/

const fs = require('fs');
const path = require('path');

readFile("data.json", (err, data) => {
    if (err) {
        console.error(`Couldnot read file ${fileName}`, err);
    } else {

        const employeesData = Object.values(JSON.parse(data))[0];
        const output1 = problem1([2, 13, 23], employeesData);
        writeFile("Output1.json", JSON.stringify(output1), (err) => {
            if (err) {
                console.error(`Couldnot write file "Output1.json"`, err);
            } else {

                const output2 = problem2(employeesData);
                writeFile("Output2.json", JSON.stringify(output2), (err) => {
                    if (err) {
                        console.error(`Couldnot write file "Output2.json"`, err);
                    } else {

                        const output3 = problem3(output2, "Powerpuff Brigade");
                        writeFile("Output3.json", JSON.stringify(output3), (err) => {
                            if (err) {
                                console.error(`Couldnot write file "Output3.json"`, err);
                            } else {

                                const output4 = problem4(employeesData, 2);
                                writeFile("Output4.json", JSON.stringify(output4), (err) => {
                                    if (err) {
                                        console.error(`Couldnot write file "Output4.json"`, err);
                                    } else {

                                        const output5 = problem5(employeesData);
                                        writeFile("Output5.json", JSON.stringify(output5), (err) => {
                                            if (err) {
                                                console.error(`Couldnot write file "Output5.json"`, err);
                                            } else {

                                                const output6 = problem6(output5, 93, 92);
                                                writeFile("Output6.json", JSON.stringify(output6), (err) => {
                                                    if (err) {
                                                        console.error(`Couldnot write file "Output6.json"`, err);
                                                    } else {
        
                                                        const output7 = problem7(employeesData);
                                                        writeFile("Output7.json", JSON.stringify(output7), (err) => {
                                                            if (err) {
                                                                console.error(`Couldnot write file "Output7.json"`, err);
                                                            } else {
                                                                console.log("all operations completed")
                                                            }
                                                        })
        
                                                    }
                                                })

                                            }
                                        })
                                    }
                                })
                            }
                        })

                    }
                })

            }
        })

    }
});


function readFile(fileName, callback) {
    if (fileName !== undefined) {
        fs.readFile(path.join(__dirname, fileName), "utf-8", (err, data) => {
            if (err) {
                callback(err);
            } else {
                console.log(`Sucessful in reading file ${fileName}`);
                callback(null, data);
            }
        })
    }
}

function writeFile(fileName, data, callback) {
    if (fileName !== undefined) {
        fs.writeFile(path.join(__dirname, fileName), data, (err) => {
            if (err) {
                callback(err);
            } else {
                console.log(`Sucessful in writing in file ${fileName}`);
                callback();
            }
        })
    }
}


// 1. Retrieve data for ids : [2, 13, 23].

function problem1(idData, data) {
    return idData.map(id => {
        return Object.values(data).find(employee => employee.id === id)
    })
}

// 2. Group data based on companies.
//         { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

function problem2(data) {
    return data.reduce((groupByCompany, employee) => {

        if (!groupByCompany.hasOwnProperty(employee.company)) {
            groupByCompany[employee.company] = [];
        }
        groupByCompany[employee.company].push(employee);
        return groupByCompany
    }, {})
}

// 3. Get all data for company Powerpuff Brigade

function problem3(data, companyName) {

    return Object.keys(data).reduce((acc, company) => {
        if (company == companyName) {
            acc = data[company]
        }
        return acc
    }, [])
}

// 4. Remove entry with id 2.

function problem4(data, idToRemove) {

    return data.reduce((acc, company) => {
        if (company.id !== idToRemove) {
            acc.push(company)
        }
        return acc
    }, [])
}

// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.

function problem5(data) {

    return data.sort((company1, company2) => {
        if (company1.company !== company2.company) {
            return company1.company < company2.company ? -1 : 1;
        } else {
            return company1.id - company2.id
        }
    }, [])
}

// 6. Swap position of companies with id 93 and id 92.

function problem6(data, id1, id2) {
    const index1 = data.indexOf(data.find(employee => employee.id === id1))
    const index2 = data.indexOf(data.find(employee => employee.id === id2))

    if (index1 >= 0 && index2 >= 0) {
        let temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    return data
}


// 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

function problem7(data) {

    return data.map(company => {
        if (company.id % 2 == 0) {
            company.birthday = new Date().toISOString().slice(0, 10);
        }
        return company
    })
}